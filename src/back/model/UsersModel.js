const usersRepository = require("../db/usersRepository");

class UsersModel {
    async registerUser(user) {
        const createdUser = await usersRepository.registerUser(user);
        return createdUser;
    }

    getUser(id) {
        return usersRepository.getUser(id);
    }

    getUserByEmail(email) {
        return usersRepository.getUserByEmail(email)
    }
}

module.exports = UsersModel;
