const profilesRepository = require("../db/profilesRepository");

class ProfilesModel {
  getProfile(userId) {
    return profilesRepository.getProfileByUserId(userId);
  }

  createProfile(profile) {
    return profilesRepository.createProfile(profile);
  }

  editProfile(profile) {
    return profilesRepository.editProfile(profile);
  }

  deleteProfile(userId) {
    return profilesRepository.deleteProfile(userId);
  }
}

module.exports = ProfilesModel;
