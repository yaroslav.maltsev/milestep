const eventRepository = require("../db/eventsRepository");
const subscribesRepository = require("../db/subscribesRepository");

class EventsModel {
    async findAll(userId) {
        const eventList = await eventRepository.getAllEvents();
        let events = [];
        for (let i = 0; i < eventList.length; i++) {
            const event = await pinSubscribeField(userId, eventList[i]);
            event.subscribed = true;
            events.push(event);
        }
        return events;
    }

    async findById(userId, id) {
        const event = await eventRepository.getEventById(id);
        return pinSubscribeField(userId, event);
    }

    findCreated(userId) {
        return eventRepository.getEventsByCreator(userId).then(list => list.map(el => {
            el.subscribed = true;
            return el;
        }));
    }

    async findMy(userId) {
        const indexes = await subscribesRepository.getUserSubscriptions(userId);
        let events = [];
        for (let i = 0; i < indexes.length; i++) {
            const event = await eventRepository.getEventById(indexes[i]);
            event.subscribed = true;
            events.push(event);
        }
        return events;
    }

    deleteById(id) {
        return eventRepository.deleteEventById(id);
    }

    async update(userId, event) {
        const updatedEvent = await eventRepository.updateEvent(event);
        return this.findById(userId, updatedEvent.id);
    }

    async create(userId, event) {
        const createdEvent = await eventRepository.createEvent(event);
        this.subscribe(userId, createdEvent.id)
        return this.findById(userId, createdEvent.id);
    }

    async subscribe(userId, eventId) {
        const exist = await subscribesRepository.recordExist(userId, eventId)
        if (!exist) {
            await subscribesRepository.addSubscription(userId, eventId);
        }
        return this.findById(userId, eventId);
    }

    async unsubscribe(userId, eventId) {
        const exist = await subscribesRepository.recordExist(userId, eventId)
        if (exist) {
            await subscribesRepository.deleteSubscription(userId, eventId);
        }
        return this.findById(userId, eventId);
    }
}

async function pinSubscribeField(userId, event) {
    const isSubscribed = await subscribesRepository.recordExist(userId, event.id);
    event.subscribed = isSubscribed;
    return event;
}

module.exports = EventsModel;