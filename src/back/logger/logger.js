function logRequest(req, res, next) {
    const currentDate = getFormattedDate(new Date());

    const method = req.method;
    const url = req.url;
    const status = res.statusCode;
    const log = `[${currentDate}] ${method}:${url} ${status}`;

    console.log(log);

    next();
}

function logError(err, req, res, next) {
    const currentDate = getFormattedDate(new Date());
    console.error(`[${currentDate}]: ${err}`);
    res.status(500).json({ message: "Something went wrong" });
}

function getFormattedDate(date) {
    return (
        date.getFullYear() +
        "-" +
        (date.getMonth() + 1) +
        "-" +
        date.getDate() +
        " " +
        date.getHours() +
        ":" +
        date.getMinutes() +
        ":" +
        date.getSeconds()
    );
}

module.exports = { logRequest, logError };
