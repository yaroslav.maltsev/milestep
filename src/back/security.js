function checkAuth(req, res, next) {
  console.log("USER: ", req.user);
  const availableToAll = ["/login", "/register"];
  if (availableToAll.includes(req.originalUrl) || req.user) {
    next();
  } else {
    res.redirect("/login");
  }
}

module.exports = checkAuth;
