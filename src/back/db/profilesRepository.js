const pool = require("./db-config");

const profiles = {
  getProfileByUserId,
  deleteProfile,
  createProfile,
};

async function getProfileByUserId(userId) {
  const queryResult = pool.query("select * from profiles where user_id = $1", [
    userId,
  ]);
  return queryResult.rows[0];
}

async function createProfile(profile) {
  const values = [profile.interests, profile.job, profile.degree, profile.goal];
  const createdUser = await pool.query(
    "insert into profiles (interests, job, degree, goal) values ($1, $2, $3, $4) returning *",
    values
  );
  return createdUser.rows[0];
}

async function editProfile(profile) {
  const values = [
    profile.interests,
    profile.job,
    profile.degree,
    profile.goal,
    profile.userId,
  ];
  const createdUser = await pool.query(
    "update profiles set interests = $1, job = $2, degree = $3, goal $4 where user_id = $5 returning *",
    values
  );
  return createdUser.rows[0];
}

function deleteProfile(userId) {
  return pool.query("delete from profiles where user_id = $1", [userId]);
}

module.exports = profiles;
