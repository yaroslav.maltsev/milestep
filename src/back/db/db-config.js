const parse = require("pg-connection-string").parse;
const { Pool } = require("pg");
const dbUrl = process.env.db_url;

const config = parse(dbUrl);

/*
tables:
    events:
        id
        creator_id
        name
        description
        date
    users:
        id
        name
        lastname
        email
        password
    subscribes:
        user_id
        event_id
    profile:
        id,
        interests,
        job,
        goal
*/

module.exports = new Pool(config);
