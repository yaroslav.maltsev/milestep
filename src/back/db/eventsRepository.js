const RecordNotFoundException = require("../exceptions/RecordNotFoundException");
const pool = require("./db-config");

const events = {
    getAllEvents,
    getEventById,
    getEventsByCreator,
    createEvent,
    updateEvent,
    deleteEventById,
    clearTable
}

async function getAllEvents() {
    const queryResult = await pool.query("select * from events");
    return queryResult.rows;

}

async function getEventById(eventId) {
    const res = await pool.query("select * from events where id = $1", [eventId]);
    if (res.rows.length < 1) {
        throw RecordNotFoundException("There are no event with such id " + eventId);
    } else {
        return res.rows[0];
    }
}

async function getEventsByCreator(creatorId) {
    const queryResult = await pool.query("select * from events where creator_id = $1", [creatorId]);
    return queryResult.rows;
}

async function createEvent(newEvent) {
    const { creatorId, name, description, date } = newEvent;
    const values = [creatorId, name, description, date]
    const createdEvent = await pool.query("insert into events (creator_id, name, description, date) values ($1, $2, $3, $4) returning *", values)
    return createdEvent.rows[0];
}

async function updateEvent(editedEvent) {
    const { id, name, description, date } = editedEvent;
    const values = [name, description, date, id]
    const updatedEvent = await pool.query("update events set name = $1, description = $2, date = $3 where id = $4 returning *", values)
    return updatedEvent.rows[0];
}

function deleteEventById(eventId) {
    return pool.query("delete from events where id = $1", [eventId]);
}

function clearTable() {
    return pool.query("delete from events");
}

module.exports = events;
