const pool = require("./db-config");

const subscribes = {
    getEventSubscribers,
    getUserSubscriptions,
    addSubscription,
    deleteSubscription,
    recordExist
}

async function getUserSubscriptions(userId) {
    const queryResult = await pool.query("select event_id from subscribes where user_id = $1", [userId]);
    return convertInIdList(queryResult.rows, "event_id");
}

async function getEventSubscribers(eventId) {
    const queryResult = await pool.query("select user_id from subscribes where event_id = $1", [eventId]);
    return convertInIdList(queryResult.rows, "user_id");;
}

async function addSubscription(userId, eventId) {
    const values = [userId, eventId];
    const createdSubscription = await pool.query("insert into subscribes (user_id, event_id) values ($1, $2) returning *", values)
    return createdSubscription.rows[0];
}

function deleteSubscription(userId, eventId) {
    const values = [userId, eventId];
    return pool.query("delete from subscribes where user_id = $1 and event_id = $2", values);
}

async function recordExist(userId, eventId) {
    const queryResult = await pool.query("select exists(select * from subscribes where user_id = $1 and event_id = $2);", [userId, eventId]);
    return queryResult.rows[0].exists ?? false;
}

function convertInIdList(objList, paramName) {
    return objList.map(obj => obj[paramName]);
}


module.exports = subscribes;
