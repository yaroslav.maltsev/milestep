const pool = require("./db-config");

const users = {
    getUser,
    getUserByEmail,
    registerUser,
    confirm
}

async function getUser(id) {
    const queryResult = pool.query("select * from users where id = $1", [id]);
    return queryResult.rows[0]
}

async function getUserByEmail(email) {
    const queryResult = await pool.query("select * from users where email = $1", [email]);
    return queryResult.rows[0]
}

async function registerUser(user) {
    const values = [
        user.name,
        user.lastname,
        user.email,
        user.password,
        false
    ]
    const createdUser = await pool.query("insert into users (name, lastname, email, password, confirmed) values ($1, $2, $3, $4, $5) returning *", values);
    return createdUser.rows[0];
}

async function confirm(userId) {
    const createdUser = pool.query("update users set confirmed = true where id = $1 returning *", [userId]);
    return createdUser.rows[0]
}

module.exports = users;