const checkAuth = require("./security");

function addAuth(app) {
  const express = require("express");
  const session = require("express-session");
  const passport = require("passport");
  const UsersModel = require("./model/UsersModel");
  const model = new UsersModel();
  const LocalStrategy = require("passport-local").Strategy;
  const flash = require("connect-flash");
  const crypto = require("crypto");

  passport.serializeUser((user, done) => done(null, user));
  passport.deserializeUser((user, done) => done(null, user));

  app.use(express.urlencoded({ extended: true }));
  app.use(session({ secret: "secret key" }));
  app.use(flash());
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(checkAuth);

  const local = new LocalStrategy(async (username, password, done) => {
    console.log("AUTH IS WORKING");
    const userFromDb = await model.getUserByEmail(username);
    if (!userFromDb || userFromDb.password != getHashedPassword(password)) {
      return done(null, false, { message: "It's impossible to authorize" });
    } else {
      console.log("SUCCESS");
      return done(null, userFromDb);
    }
  });

  passport.use(local);

  app.get("/login", (req, res) => {
    res.send("Login page. Please, authorize.");
  });

  app.post("/logout", function (req, res) {
    req.logout();
    res.redirect("/");
  });

  app.post(
    "/login",
    passport.authenticate("local", {
      successRedirect: "/",
      failureRedirect: "/login",
      failureFlash: true,
    })
  );

  const getHashedPassword = (password) => {
    const sha256 = crypto.createHash("sha256");
    const hash = sha256.update(password).digest("base64");
    return hash;
  };

  return app;
}

module.exports = addAuth;
