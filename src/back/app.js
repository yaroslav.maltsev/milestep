const express = require("express");
const router = require("./routes/routes");
const logger = require("./logger/logger");
const addAuth = require("./auth");

let app = express();

app.use(express.json());
app.use(logger.logRequest);
app = addAuth(app);
app.use(router);
app.use(logger.logError);

module.exports = app;
