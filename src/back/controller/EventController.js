const EventModel = require("../model/EventsModel")
const model = new EventModel();

class EventsController {
    findAll() {
        return model.findAll();
    }

    findById(userId, id) {
        return model.findById(userId, id);
    }

    findCreated(userId) {
        return model.findCreated(userId);
    }

    findMy(userId) {
        return model.findMy(userId);
    }

    deleteById(id) {
        return model.deleteById(id);
    }

    update(event) {
        return model.update(event)
    }

    subscribe(userId, eventId) {
        return model.subscribe(userId, eventId)
    }

    unsubscribe(userId, eventId) {
        return model.unsubscribe(userId, eventId)
    }

    create(userId, event) {
        return model.create(userId, event);
    }
}

module.exports = EventsController;