const ProfileModel = require("../model/ProfilesModel");
const model = new ProfileModel();

class ProfileController {
  getProfile(userId) {
    return model.getProfile(userId);
  }

  createProfile(profile) {
    return model.createProfile(profile);
  }

  deleteProfile(userId) {
    return model.deleteProfile(userId);
  }

  editProfile(profile) {
    return model.editProfile(profile);
  }
}

module.exports = ProfileController;
