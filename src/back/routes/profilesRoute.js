const router = require("express").Router({ mergeParams: true });
const ProfileController = require("../controller/ProfileController");

const controller = new ProfileController();

router.get("/", (req, res, next) => {
  controller
    .getProfile(req.user.id)
    .then((result) => res.json(result))
    .catch(next);
});

router.delete("/", (req, res, next) => {
  controller
    .deleteProfile(req.user.id)
    .then((result) => res.json(result))
    .catch(next);
});

router.post("/", (req, res, next) => {
  req.body.userId = req.user.id;
  controller
    .createProfile(req.body)
    .then((result) => res.json(result))
    .catch(next);
});

router.patch("/", (req, res, next) => {
  req.body.userId = req.user.id;
  controller
    .editProfile(req.body)
    .then((result) => res.json(result))
    .catch(next);
});

module.exports = router;
