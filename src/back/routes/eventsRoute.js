const router = require("express").Router({ mergeParams: true });

const EventController = require("../controller/EventController");
const RecordNotFoundException = require("../exceptions/RecordNotFoundException");

const controller = new EventController();

router.get("/", (req, res, next) => {
  controller
    .findAll(req.user.id)
    .then((result) => res.json(result))
    .catch(next);
});

router.get("/:eventId", (req, res, next) => {
  const eventId = req.params.eventId;
  controller
    .findById(req.user.id, eventId)
    .then((result) => res.json(result))
    .catch((err) => {
      if (err instanceof RecordNotFoundException) {
        return res.json({
          message: `Event with id ${eventId} cannot be found!`,
        });
      } else {
        throw err;
      }
    })
    .catch(next);
});

router.delete("/:eventId", (req, res, next) => {
  const eventId = req.params.eventId;
  controller
    .deleteById(eventId)
    .then((result) => res.json(result))
    .catch((err) => {
      if (err instanceof RecordNotFoundException) {
        return res.json({
          message: `Event with id ${eventId} cannot be found!`,
        });
      } else {
        throw err;
      }
    })
    .catch(next);
});

router.post("/", (req, res, next) => {
  req.body.creatorId = req.user.id;
  controller
    .create(req.user.id, req.body)
    .then((result) => res.json(result))
    .catch((err) => {
      if (err instanceof RecordNotFoundException) {
        return res.json({
          message: `Event with id ${eventId} cannot be found!`,
        });
      } else {
        throw err;
      }
    })
    .catch(next);
});

router.post("/:eventId/subscribe", (req, res, next) => {
  const eventId = req.params.eventId;
  controller
    .subscribe(req.user.id, eventId)
    .then((result) => res.json(result))
    .catch((err) => {
      if (err instanceof RecordNotFoundException) {
        return res.json({
          message: `Event with id ${eventId} cannot be found!`,
        });
      } else {
        throw err;
      }
    })
    .catch(next);
});

router.post("/:eventId/unsubscribe", (req, res, next) => {
  const eventId = req.params.eventId;
  controller
    .unsubscribe(req.user.id, eventId)
    .then((result) => res.json(result))
    .catch((err) => {
      if (err instanceof RecordNotFoundException) {
        return res.json({
          message: `Event with id ${eventId} cannot be found!`,
        });
      } else {
        throw err;
      }
    })
    .catch(next);
});

router.patch("/:eventId", (req, res, next) => {
  const eventId = req.params.eventId;
  req.body.id = eventId;
  controller
    .update(req.user.id, req.body)
    .then((result) => res.json(result))
    .catch((err) => {
      if (err instanceof RecordNotFoundException) {
        return res.json({
          message: `Event with id ${eventId} cannot be found!`,
        });
      } else {
        throw err;
      }
    })
    .catch(next);
});

module.exports = router;
