const express = require("express");
const router = express.Router({ mergeParams: true });

const eventsRoute = require("./eventsRoute");
const createdRoute = require("./createdRoute");
const usersRoute = require("./usersRoute");

router
  .use("/events", eventsRoute)
  .use("/", createdRoute)
  .use("/profile", createdRoute)
  .use("/register", usersRoute);

module.exports = router;
