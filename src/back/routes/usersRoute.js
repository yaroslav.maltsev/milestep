const router = require("express").Router({ mergeParams: true });
const EventController = require("../model/UsersModel");
const crypto = require("crypto");

const controller = new EventController();

router.post("/", (req, res, next) => {
  req.body.id = req.user.id;
  req.body.password = getHashedPassword(req.body.password);
  controller
    .registerUser(req.body)
    .then((result) => res.json(result))
    .catch(next);
});

const getHashedPassword = (password) => {
  const sha256 = crypto.createHash("sha256");
  const hash = sha256.update(password).digest("base64");
  return hash;
};

module.exports = router;
