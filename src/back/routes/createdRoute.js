const router = require("express").Router({ mergeParams: true });

const EventController = require("../controller/EventController");

const controller = new EventController();

router.get("/created", (req, res, next) => {
  // events which i created
  controller
    .findCreated(req.user.id)
    .then((result) => res.json(result))
    .catch(next);
});

router.get("/", (req, res, next) => {
  // events on which i subscribed
  controller
    .findMy(req.user.id)
    .then((result) => res.json(result))
    .catch(next);
});

module.exports = router;
